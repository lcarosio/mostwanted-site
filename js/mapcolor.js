var map;
var MY_MAPTYPE_ID = 'MostWanted';

function initialize() {
		
	var stylez = [
		{
			featureType: 'water',
			elementType: 'all',
			stylers: [
				{ hue: '#cccccc' },
				{ saturation: -100 },
				{ lightness: 17 },
				{ visibility: 'on' }
			]
		},{
			featureType: 'landscape',
			elementType: 'all',
			stylers: [
				{ hue: '#dddddd' },
				{ saturation: -100 },
				{ lightness: -3 },
				{ visibility: 'on' }
			]
		},{
			featureType: 'landscape.man_made',
			elementType: 'all',
			stylers: [
				{ hue: '#eeeeee' },
				{ saturation: -100 },
				{ lightness: 39 },
				{ visibility: 'on' }
			]
		},{
			featureType: 'landscape.natural',
			elementType: 'all',
			stylers: [
				{ hue: '#aaaaaa' },
				{ saturation: -100 },
				{ lightness: -30 },
				{ visibility: 'on' }
			]
		},{
			featureType: 'road',
			elementType: 'all',
			stylers: [
				{ hue: '#cccccc' },
				{ saturation: -100 },
				{ lightness: 44 },
				{ visibility: 'on' }
			]
		},{
			featureType: 'road.highway',
			elementType: 'geometry',
			stylers: [
				{ hue: '#ffffff' },
				{ saturation: -100 },
				{ lightness: 100 },
				{ visibility: 'on' }
			]
		},{
			featureType: 'road.local',
			elementType: 'all',
			stylers: [
				{ hue: '#cccccc' },
				{ saturation: -100 },
				{ lightness: -20 },
				{ visibility: 'on' }
			]
		},{
			featureType: 'poi',
			elementType: 'all',
			stylers: [
				{ hue: '#bbbbbb' },
				{ saturation: -100 },
				{ lightness: -6 },
				{ visibility: 'on' }
			]
		},{
			featureType: 'transit',
			elementType: 'all',
			stylers: [
				{ hue: '#cccccc' },
				{ saturation: 0 },
				{ lightness: 20 },
				{ visibility: 'on' }
			]
		},{
			featureType: 'administrative',
			elementType: 'all',
			stylers: [
				{ hue: '#666666' },
				{ saturation: 0 },
				{ lightness: -22 },
				{ visibility: 'on' }
			]
		}
	];
	
	var mapOptions = {
		zoom: 14,
		center: new google.maps.LatLng(50.792531, 4.330962),
		disableDefaultUI: true,
		mapTypeControlOptions: {
			mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
		},
		mapTypeId: MY_MAPTYPE_ID
	};

	map = new google.maps.Map(document.getElementById("map_canvas"),
		mapOptions);

	var styledMapOptions = {
		name: "MostWanted - Wild Digital Agency"
	};
	
	var image = '../images/googlemap-icon.png';
	
	var myLatLng = new google.maps.LatLng(50.792531, 4.330962);
	
	var beachMarker = new google.maps.Marker({
		position: myLatLng,
		map: map,
		icon: image,
		draggable:false,
		animation: google.maps.Animation.DROP,
		title: 'MostWanted  - Wild Digital Agency'
	});
	
	var contentString = '<div id="siteNotice">'+
		'<h4>MostWanted</h4>'+
		'<h5>Wild Digital Agency</h5>'+
		'<p class="address">RUE EGIDE VAN OPHEM, 40 A<br>1180 BRUXELLES - BELGIUM</p>'+
		'<p><strong>Tél</strong> +32 2 344 20 44 &nbsp; <strong>fax</strong> +32 2 344 50 44</p>'+
		'</div>';

	var infowindow = new google.maps.InfoWindow({
		content: contentString
	});

	google.maps.event.addListener(beachMarker, 'click', function() {
		infowindow.open(map,beachMarker);
	});
	
	var jayzMapType = new google.maps.StyledMapType(stylez, styledMapOptions);
	map.mapTypes.set(MY_MAPTYPE_ID, jayzMapType);

	function toggleBounce() {

		if (marker.getAnimation() != null) {
			marker.setAnimation(null);
		} else {
			marker.setAnimation(google.maps.Animation.BOUNCE);
		}
	}
	
}
