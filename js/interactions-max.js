var itsaIpad = true,
	checkFadeInBulle = false,
	scrollTop = $(window).scrollTop(),
	wH = $(window).height();

function startUp(){
	if( String(navigator.userAgent).search(/(iPad)|(iPhone)/i) > 0 ){
		itsaIpad = false;
	}
}

function checkFadeIn(scrollTop, wH) {
	if (itsaIpad) {
		var myDelay = 0;
		$('.bloc').each(function() {
			var $this = $(this),
				elemTop = $this.offset().top;
			myDelay++;
			if ( $this.attr('id') == 'footer' || $this.hasClass('.accounts') ) myDelay = 0;
			if ($this.css('opacity')=== '0' && (parseInt(elemTop) <= parseInt(scrollTop + wH - 50)) ) {
				if (checkFadeInBulle) {
					$this.animate({ marginTop: 0 }, 1500, 'easeOutElastic')
					.animate({ opacity: 1 }, { duration: 1500, queue: false });
				} else {
					$this.delay(myDelay*50).fadeTo(400, 1);
				}
			}
		});
	} else {
		$('.bloc').css('opacity','1');
	}
}

jQuery.extend(
	jQuery.expr[ ":" ],
	{ reallyvisible : function (a) { return !(jQuery(a).is(':hidden') || jQuery(a).parents(':hidden').length); }}
); 

if (($.browser.msie  && parseInt($.browser.version, 10) === 7) || ($.browser.msie  && parseInt($.browser.version, 10) === 8)) {
	var ie7 = true;
}

function scrollWin(myTarget){
	/**IPAD**/
	if (!itsaIpad) {
	
		$('body,html,document').animate({
			scrollTop: myTarget.offset().top-30
		}, 1000);
	
	} else {
		$('html,body').animate({
			scrollTop: myTarget.offset().top-30
		}, 1000);
	}
}

$(document).ready(function() {
	
	startUp();
	
	var elemen,
		destination,
		side,
		nesspressoCount,
		nesspressoTime,
		maxHeight = 0,
		calls = 1,
		i = 0,
		$portfolio,
		bodyClass = $('body').attr('class'),
		popin = $('.popinContainer');
	
	//Fade elements on scroll
	checkFadeIn(scrollTop, wH);
	$(window).scroll(function(){
		scrollTop = $(window).scrollTop();
		wH = $(window).height();
		if ( bodyClass === "testimonies" || bodyClass === "testimonies nl" ) checkFadeInBulle = true;
		checkFadeIn(scrollTop, wH);
	});
	
	// POPIN SECURE
	$('.options .access').on('click', function() {
		popin.fadeIn();
		popin.children('.popin').animate({
			top: '50%'
		},600);
	});
	
	popin.children('.popin').children('.close').on('click', function() {
		popin.children('.popin').animate({
			top: '-50%'
		},600);
		popin.delay(100).fadeOut(400, function() {
			popin.find('.errorMessage').hide();
			popin.find('form').show();
		});
		
	});
	
	popin.find('label.button.red').on('click', function() {
		popin.find('form').hide();
		popin.find('.errorMessage').show();
	});
	
	popin.find('.errorMessage a').on('click', function() {
		popin.find('.errorMessage').hide();
		popin.find('form').show();
	});







	if ( bodyClass === 'homepage' || bodyClass === 'homepage nl' ) {	
			
		
			var options = {
				autoPlay: true,
				reverseAnimationsWhenNavigatingBackwards: true,
				swipePreventsDefault: true,
				swipeEvents: {left: "next", right: "prev", up: false, down: false},
				preloader: "#sequence-preloader",
				prependPreloader: false,
				prependPreloadingComplete: "#sequence-preloader, .slides_container",
				animateStartingFrameIn: false, 
				transitionThreshold: 0,
				
				afterLoaded: function() {				
					$(".pagination ul").fadeIn(1000);
					$(".pagination li:nth-child("+(sequence.settings.startingFrameID)+")").addClass("current");
					
					if(!sequence.transitionsSupported){
						$(".slides_container").animate({"opacity": "1"}, 1000);
					}
				},
				beforeNextFrameAnimatesIn: function(){
					$(".pagination li:not(:nth-child("+(sequence.nextFrameID)+"))").removeClass("current");
					$(".pagination li:nth-child("+(sequence.nextFrameID)+")").addClass("current");
				}
			};
			
			
			$(".pagination li").click(function(event){
				event.preventDefault();
				if(!sequence.active){
					$(this).removeClass("current").addClass("current");
					sequence.nextFrameID = $(this).index()+1;
					sequence.goTo(sequence.nextFrameID);
				}
			});
			
			
			var sequence = $(".slides_container").sequence(options).data("sequence");
			if(!sequence.transitionsSupported || sequence.prefix == "-o-"){
				$(".slides_container").animate({"opacity": "1"}, 1000);
				sequence.preloaderFallback();
			}
			
			
	}










	if ( bodyClass === 'about' || bodyClass === 'about nl' ) {	
		
		/*
			ABOUT - TEAM
		*/
		
		var leftOrRight = false;
		var timer;
		var whereTo = "";
		
		var $carousel = $('#carousel_ul');
		var howManyGringos = $carousel.find('.item').index()+1;
		$('.citation p strong').text(howManyGringos);
		
		$carousel.find('li:first').before($carousel.find('li:last'));
		$carousel.find('.item:not(.visible) img').css('opacity','0.2')
		
		function mySlide() {
			
			var item_width = $carousel.find('li').outerWidth();
									
			if (whereTo == 'left'){
				var left_indent = parseInt($carousel.css('left'));
				
			} else {
				var left_indent = parseInt($carousel.css('left')) - item_width;
			}
				
			var ease = "swing";
			
			if(leftOrRight) { 
				ease = "linear"
			}
						
			if ( $carousel.is(':not(:animated)') ){
				
				if (whereTo == 'left'){
					$carousel.find('li:first').before($carousel.find('li:last'));
					$carousel.css({left: -2*item_width});
				}
				
				$carousel.animate({
					left: left_indent
				},{
					duration:500, 
					easing:ease,
					complete:function(){	
									
						if (whereTo == 'right'){
							$carousel.find('li:last').after($carousel.find('li:first')); 
							$carousel.css({left: -item_width});
						}
						
					
						$carousel.find('li').each(function() {
							var leftPosition = $(this).position().left;
							
							if ( leftPosition == 458 || leftPosition == 916 ) {
								$('img',this).animate({
									opacity: 1
								},500, function() {
								}).parent().addClass('visible');
								
							} else {
								$('img',this).animate({
									opacity: 0.2
								},500).parent().removeClass('visible');
							}
						});
						
						if(leftOrRight) {						
							timer = setTimeout(mySlide, 0);						
						}											
					}
				});
			
			}
		}
		
		
		$('#carousel_container .nextRollOver').css('opacity', 0).mouseenter(function() {
			whereTo = "right";
			leftOrRight = true;
			mySlide();		
		}).mouseleave(function() {
			leftOrRight = false;
		});
		
		$('#carousel_container .prevRollOver').css('opacity', 0).mouseenter(function() {
			whereTo = "left";
			leftOrRight = true;
			mySlide();
		}).mouseleave(function() {
			leftOrRight = false;
		});
		

		$carousel.find('.item').hover(function() {
			var btm = -540;				
			if($(this).is($("#carousel_container li#independants")[0]))	{
				btm = 0;
			}
			if ( $(this).hasClass('visible') && $(this).is(':not(:animated)')) {
				$('.normal, .infos',this).stop(true,true);
				$('.normal',this).fadeIn(500);
				$('.infos',this).animate({
					bottom: btm
				},400, 'easeOutCirc');
			}
			$('.moreDetails', this).show();
		}, function() {
			$('.normal, .infos',this).stop(true,true);
			$('.normal',this).fadeOut(500);
			var btm = -630;				
			if($(this).is($("#carousel_container li#independants")[0])) {
				btm = -201;
			}
			$('.infos',this).animate({
				bottom: btm
			},400, 'easeOutCirc');			
		});
		
		
		$carousel.find('.moreDetails').click(function(event) {
			event.preventDefault();
			$(this).parent().animate({
					bottom: 0
				},400, 'easeOutCirc');
			$(this).hide();
		});
		
		

		
		
		/**IPAD**/
		if (!itsaIpad) {
			$('#carousel_container').css({
				width: 912,
				overflow:'hidden'
			});
			
			$('#carousel_container').touchwipe({
				preventDefaultEvents: true,
				wipeLeft: function() { whereTo = "right"; mySlide();	 },
				wipeRight: function() { whereTo = "left"; mySlide();	 }
			});
		}
	}
	
	
	
	
	
	
	
	
	
	

	if ( bodyClass === 'services' || bodyClass === 'services nl' ) {
		
		var $services = $('#servicesAnatomy');
		var $servicesSlides = $('#servicesSlides ul');
		var mapHover;
		var mapClick;
		
		$services.find('.servicesAnatomyItems li').hover(function() {
			var servicesId = $(this).attr('class');
			
			$services.find('.mask').show();
			$services.find('.mask span:not(.'+servicesId+')').stop(true,true).fadeOut();
			$services.find('.mask span.'+servicesId).stop(true,true).fadeIn();
			$services.find('.servicesAnatomyItems li:not(.'+servicesId+') a').stop(true,true).animate({
				opacity: 0.3
			},400);
			
			$(this).find('.icon').css({
				backgroundPositionY: -164
			});
			
		}, function() {
			$services.find('.mask span').stop(true,true).fadeOut();
			$(this).find('.icon').css({
				backgroundPositionY: ''
			});
		});
		
		
		/*MAP AREA trigger hover and click events*/
		
		$services.find('.traits .map area').hover(function(){
			var servicesId = $(this).attr('class');
			$services.find('.servicesAnatomyItems li.'+servicesId).trigger('mouseover')

		}, function() {
			var servicesId = $(this).attr('class');
			$services.find('.servicesAnatomyItems li.'+servicesId).trigger('mouseleave')
		});
		
		$services.find('.traits .map area').on('click', function(e) {
			e.preventDefault();
		});
		
		$services.find('.servicesAnatomyItems li').on('click', function(e) {
			e.preventDefault();
			mapClick = $(this).attr('class');
			$services.find('.traits .map area.'+mapClick).trigger('click');
		});
				
		/*prepare for loop*/
		$servicesSlides.find('li:first').before($servicesSlides.find('li:last'));
		
		//Styles
		function servicesStyles() {
			$servicesSlides.find('li:not(.visible)').animate({ opacity: 0.2 },300);
			$servicesSlides.find('li.visible').animate({ opacity: 1 },300);
			$servicesSlides.find('li.visible:first').css({ backgroundColor: '#282828'});
			$servicesSlides.find('li.visible:last').css({ backgroundColor: '#1d1d1d'});
		}
		
		//applying styles on load
		servicesStyles();
		
		function servicesSlider(direction,goTo) {
			var itemWidth = 458;
			var currentPosition = parseInt($servicesSlides.css('left'));
			var maxIndex = $servicesSlides.find('li:last').index();
			
			if (goTo != undefined) {
				
				/* GOTO - Direct to a slide */
				
				var goToElement = $servicesSlides.find('#'+goTo);
				var goToPosition = goToElement.position().left;
								
				if ( $servicesSlides.is(':not(:animated)') ) {
					
					$servicesSlides.animate({
						left: -goToPosition
					},500, function() {
					
						if ( goToElement.index() == 0 )
						
						{
							$servicesSlides.find('li:first').before($servicesSlides.find('li:last'));
							$servicesSlides.css({left: -itemWidth});
						}
						
						else if ( goToElement.index() == (maxIndex-1) )
						
						{
							$servicesSlides.find('li:last').after($servicesSlides.find('li:first'));
							$servicesSlides.css({left: (-goToPosition+itemWidth) });
						}
						
						else if ( goToElement.index() == maxIndex )
						
						{
							$servicesSlides.find('li:last').after($servicesSlides.find('li:first'));
							$servicesSlides.css({left: (-goToPosition+itemWidth) });
							//double effect :s
							$servicesSlides.find('li:last').after($servicesSlides.find('li:first'));
							$servicesSlides.css({left: (-goToPosition+itemWidth+itemWidth) });
						}
						
						$servicesSlides.find('li').removeClass('visible');
						goToElement.addClass('visible');
						goToElement.next().addClass('visible');
						
						servicesStyles();
					});
				}
				
			} else {
				
				/* Next & Prev */
				
				if (direction === "right") {
					var itemWidth = -itemWidth;
				}
				
				if ( $servicesSlides.is(':not(:animated)') ) {
				
					$servicesSlides.animate({
						left: currentPosition+itemWidth
					},500, function() {
					
						//get new position
						currentPosition = parseInt($servicesSlides.css('left'));
						
						/* infinite loop */					
						if (direction === 'left') {
							
							$servicesSlides.find('li:first').before($servicesSlides.find('li:last'));
							$servicesSlides.css({left: currentPosition-itemWidth });
							
							$servicesSlides.find('li.visible:first').prev().addClass('visible');
							$servicesSlides.find('li.visible:last').removeClass('visible');
							
						} else {
							$servicesSlides.find('li:last').after($servicesSlides.find('li:first'));
							$servicesSlides.css({left: currentPosition-itemWidth });
							
							$servicesSlides.find('li.visible:first').removeClass('visible');
							$servicesSlides.find('li.visible:last').next().addClass('visible');
						}
						
						servicesStyles();
					});
				}
			}
			
			
		}
		//end function
		
		$('#servicesSlides .next').on('click',function(e) {
			e.preventDefault();
			servicesSlider('right');
		});
		
		$('#servicesSlides .prev').on('click',function(e) {
			e.preventDefault();
			servicesSlider('left');
		});
		
		$services.find('.traits .map area').on('click',function(e) {
			e.preventDefault();
			servicesSlider('',$(this).attr('class'));
			scrollWin($('#servicesSlides'));
		});
		
		$('#moreServices a').on('click',function(e) {
			e.preventDefault();
			servicesSlider('',$(this).parent().attr('class'));
			scrollWin($('#servicesSlides'));
		});
		
		

		
		
		/**IPAD**/
		if (!itsaIpad) {
			$('#servicesSlides .next').css('right',0);
			$('#servicesSlides').css({
				overflow: "hidden"
			});
		
			$('.services #content').css({
				width: 960,
				overflow: "hidden"
			});
		
			$('#servicesSlides').touchwipe({
				preventDefaultEvents: true,
				wipeLeft: function() { $('#servicesSlides .next').trigger('click') },
				wipeRight: function() { $('#servicesSlides .prev').trigger('click') }
			});
			
			$('#servicesAnatomy a').hover(function(){
				$(this).trigger('click');
				$(this).trigger('click');
			});
			
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	if ( bodyClass === 'portfolio' || bodyClass === 'portfolio nl' ) {
		
		var $filter = $('.filter'),
			filterHeight = $filter.find('li').length * 24,
			$portfolio = $('.portfolioContent'),
			$footerBloc = $('#footer, #footer .bloc'),
			$p_carousel = $('#carouselInner ul'),
			$carouselLi = $('#carouselContainer ul li'),
			theId = "",
			leftOrRight = false,
			whereTo = "";
		
		//PORTFOLIO items
		$('.portfolioContent .bloc').hover(function() {
			$('h2',this).animate({
				top: -60
			},300, "easeInSine");
		}, function() {
			$('h2',this).animate({
				top: 0
			},800, "easeOutSine");
		});
		
		//ISOTOPE JS
		$portfolio.isotope({
			itemSelector: '.bloc',
			masonry: {
				columnWidth: 229
			}
		});
		
		$.param({ filter: '.cms' });
		
		//filter - list grow
		$filter.hover(function() {
			$('ul', this).stop().animate({
				height: filterHeight+1
			},300);
		}, function() {
			$('ul', this).stop().animate({
				height: 0
			},300);
		});
		
		//filter tri
		$filter.find('span:not(.filterTitle)').on('click', function() {
			var selector = $(this).data('filter'),
				$this = $(this);
									
			$filter.find('.filterTitle').text($(this).text());
			$footerBloc.css('opacity','1');
			
			$portfolio.isotope({ filter: selector });
			$portfolio.find('.bloc').css('opacity','1');
			
			location.hash = '#filter='+selector;
			
			//IE debug
			if ( $this.parents('.isotope-item').hasClass('isotope-hidden') ) {
				return;
			}
			
			if (ie7) {
				$('.isotope-hidden').fadeOut();
				$(':not(.isotope-hidden)').fadeIn();
			}
		});
		
		
		function loadImages($elem) {
			$('#circleG').remove();
			if($elem == undefined) return;
			if( $elem.attr("src") == "../images/spacer.gif" ) {
				// add the preloader
				$elem.parent().parent().parent().prepend('<div id="circleG"><div id="circleG_1" class="circleG"></div><div id="circleG_2" class="circleG"></div><div id="circleG_3" class="circleG"></div></div>');
				$('#circleG').fadeIn();
				$elem.attr("src", $elem.data('original')).load(function(){
					//if($(this).parent().parent().parent().hasClass('visible') && $(this).parent().parent().parent().parent().is(':not(:animated)') )
					$(this).fadeIn();
					$('#circleG').fadeOut("slow", function() {
						$(this).remove();
					});
				});
				$elem.css('opacity', 1);
			}
		}
		
		
		function showPorfolioDetailsArea() {		
			$('.portfolio #content').css('padding-bottom', '0');
			$('.portfolio #content .backtotop').css('display', 'none');
			$('.portfolio #porfolioDetailContainer').css('display', 'block');		
		}
				
		function hidePorfolioDetailsArea() {	
			$.scrollTo( '0px', 800, {axis:'y'} );	
			$('.portfolio #porfolioDetailContainer').fadeOut('fast', function() {				
				$('.portfolio #content').css('padding-bottom', '80px');
				$('.portfolio #content .backtotop').css('display', 'block');
			});	
		}
		
		

		
		
		function checkScreenshotSize() {
			
			$('#carouselInner ul li div.imagesContainer').unbind("mousemove").css('cursor', 'default');
			
			var mouseY, destY, totalContentH;
						
			totalContentH = $('#carouselInner ul li.visible div.imagesContainer div:first img').height();
			
			$mainContainer=$('#carouselInner ul li.visible div.imagesContainer');
			$imgContainer=$('#carouselInner ul li.visible div.imagesContainer div:first');
		 
			containerHeight = $mainContainer.height();
			
			if(totalContentH > containerHeight) {
				$mainContainer.css('cursor', 'n-resize');
			} else {
				$mainContainer.css('cursor', 'default');
			}
								
			function MouseMove(e){
				if($mainContainer.is(':not(:animated)')) {
					//mouseY=(e.pageY - $mainContainer.offset().top);	
					// ie 10 pageY bug workaround;  for now pageY is the same as clientY 
					mouseY = $(window).scrollTop() + e.clientY - $mainContainer.offset().top;
										
					
					destY = -Math.round((totalContentH - 2*containerHeight)*mouseY / containerHeight);			
										
					$imgContainer.stop().animate({top: destY-mouseY}, 1500, "easeOutCirc"); 
					
				}
			}
		 	if(totalContentH > containerHeight) {
				$mainContainer.bind("mousemove", function(event){
					MouseMove(event);
				});
			}
			
			
		}
		
		
		function selectFirstOne() {
			
			$('div.imagesContainer div img.colorImage').css('opacity', '0');
					
			$p_carousel.find('li.selected').each(function() {
				if(this.id == theId) {	
					$p_carousel.find('li.selected:first').addClass('visible');
					$p_carousel.find('li.selected:first div.imagesContainer div img.colorImage').css('opacity', '1');	
					$p_carousel.find('li.selected:first').before($p_carousel.find('li.selected:last'));
					return false;
				}
					
				$p_carousel.find('li.selected:last').after($p_carousel.find('li.selected:first')); 
				
			});		
			
		}
		
		
		function filterCarouselContent(categ) {
			
			$carouselLi.hide();
			$p_carousel.find('li').each(function() {
				$(this).removeClass('visible selected');//.children().css('opacity', '0.2');
			});
			$carouselLi.tsort({ attr: 'data-index', order: 'desc' });			
			$carouselLi.filter(categ).show().addClass('selected');
			
			if(theId == "" ) {
				 theId = $p_carousel.find('li.selected:first').attr('id');
			}
			
			selectFirstOne();					
					
			hidePorfolioDetailsArea();
			
			theId = "";
		}
		
		
		function checkVerticalNav() {
			nr_screenshots = $('#carouselInner ul li.visible div.imagesContainer').find('div').index()+1;
			if(nr_screenshots > 1) {
				$('#porfolioDetailHead #nav .up').animate({opacity: 1},300 );
				$('#porfolioDetailHead #nav .down').animate({opacity: 1},300 );
			} else {
				$('#porfolioDetailHead #nav .up').animate({opacity: .2},300 );
				$('#porfolioDetailHead #nav .down').animate({opacity: .2},300 );
			}
			
			
			// change ttl and desc
			var pId = $('#carouselInner ul li.visible').attr('id');
			$('#porfolioDetailHead #ttl div.jobTitle').each(function(index, element) {
                if(this.id == pId) {
					$(this).css('display', 'block');
				} else {
					$(this).css('display', 'none');					
				}
            });
			$('#porfolioDetailContent div.jobContent').each(function(index, element) {
            	if(this.id == pId) {
					$(this).css('display', 'block');
				} else {
					$(this).css('display', 'none');
				}
            });
			
			
			
			// check size <> 
			checkScreenshotSize();
			
			// load color images
			$p_carousel.find('li.visible .imagesContainer div').each(function() {
				loadImages($(this).find('img').first());
			});
		}
		
		
		
		
		$(window).bind( 'hashchange', function( event ){
			var hashOptions = $.deparam.fragment();
			
			$portfolio.isotope( hashOptions );
			
			if ( hashOptions.filter !== undefined ) {
			
				var filterText = hashOptions.filter.replace( /^./, '' );
				
				if ( bodyClass === 'portfolio' ) {
					switch (filterText) {
						case 'geo': filterText = 'Géo-localisation'; break;
						case 'ebusiness': filterText = 'E-Business'; break;
						case 'social': filterText = 'Social Network'; break;
						case 'mobile': filterText = 'Mobile Device'; break;
						case 'site': filterText = 'Site officiel de la marque'; break;
						case '': filterText = 'Aucun'; break;
					}
				} else {
					switch (filterText) {
						case 'concours': filterText = 'Wedstrijden'; break;
						case 'actions': filterText = 'Acties'; break;
						case 'geo': filterText = 'Geolocalisatie'; break;
						case 'ebusiness': filterText = 'E-Business'; break;
						case 'social': filterText = 'Sociaal Netwerk'; break;
						case 'mobile': filterText = 'Mobiele toestellen'; break;
						case 'site': filterText = 'Website van het merk'; break;
						case '': filterText = 'Geen'; break;
					}
				}
				
				$filter.find('.filterTitle').text(filterText);
				$portfolio.find('.bloc').css('opacity','1');
				$footerBloc.css('opacity','1');
							
				filterCarouselContent(hashOptions.filter);
				checkVerticalNav();	
				
			} else {
				location.hash = '#filter=*';	
			}
		}).trigger('hashchange');
		
		
		
		
		
				
		$('div.imagesContainer div img.greyImage').css('opacity','0.2');	
		
		function pSlide() {
				
			var item_width = $p_carousel.find('li').outerWidth();
			var item_height = $p_carousel.find('li').outerHeight();
			var left_indent = 0;
			var top_indent = 0;
			var nr_screenshots = $('#carouselInner ul li.visible div.imagesContainer').find('div').index()+1;
									
			if (whereTo == 'left') {
				left_indent = parseInt($p_carousel.css('left'));				
			} else if (whereTo == 'right') {
				left_indent = parseInt($p_carousel.css('left')) - item_width;
			} else if (whereTo == 'up') {
				top_indent = parseInt($p_carousel.css('top'));	
			} else if (whereTo == 'down') {
				top_indent = parseInt($p_carousel.css('top')) - item_height;
			}
			
			var ease = "swing";
			var duration = 500;
			
			if(leftOrRight) { 
				ease = "linear";
				duration = 1000;
			}
						
			if ( $p_carousel.is(':not(:animated)') ){
				
				if(whereTo == 'left' || whereTo == 'right') {
				
					if (whereTo == 'left'){
						$p_carousel.find('li:visible:first').before($p_carousel.find('li:visible:last'));
						$p_carousel.css({left: -2*item_width});
					}
										
					$p_carousel.animate({
						left: left_indent
					}, {
						duration:duration, 
						easing:ease,
						complete:function(){			
										
							if (whereTo == 'right'){
								$p_carousel.find('li:visible:last').after($p_carousel.find('li:visible:first')); 
								$p_carousel.css({left: -item_width});
							}
							
							
							
							$p_carousel.find('li').each(function() {
								var leftPosition = $(this).position().left;
														
								if ( leftPosition == 918) {								
									$(this).addClass('visible');								
									$('div.imagesContainer div img.colorImage', this).delay(150).animate({opacity: 1}, 500);
									
								} else {
									$(this).removeClass('visible');
									$('div.imagesContainer div:first img.colorImage',this).delay(150).animate({opacity: 0}, 500);								
								}
							});						
							checkVerticalNav();
							
							if(leftOrRight) {							
								setTimeout(pSlide, 10);
							}
						}			
					});
				}
				
				else 
				{
					
					var $current_project = $('#carouselInner ul li.visible div.imagesContainer');
									
					if (nr_screenshots == 1 ||  $current_project.is(':animated') ) 
						return;
						
					
					$current_project.find('div img:first').css('opacity', 1);
										
					if (whereTo == 'up'){
						$current_project.find('div:first').before($current_project.find('div:last'));						
						$current_project.css({top: - item_height});						
					}
					
					
					var $vAnimatedDiv = $('#carouselInner ul li.visible div.imagesContainer div')
					$vAnimatedDiv.stop();
															
					$current_project.animate({
						top: top_indent
					},500,function(){	
						
						$vAnimatedDiv.css('top', 0);
										
						if (whereTo == 'down') {
							$current_project.find('div:last').after($current_project.find('div:first'));							
						}
						$current_project.css({top: 0});		
						
						// check size ^\/;
						checkScreenshotSize();
					});
					
				
				}
				
			}
		}
		
		
		$('#porfolioDetailHead #nav a.next').on('click',function(event) {
			event.preventDefault();
			whereTo = "right";
			pSlide();
		});
		$('#porfolioDetailHead #nav a.prev').on('click',function(event) {
			event.preventDefault();
			whereTo = "left";
			pSlide();
		});
		$('#porfolioDetailHead #nav a.up').on('click',function(event) {
			event.preventDefault();
			whereTo = "up";
			pSlide();
		});
		$('#porfolioDetailHead #nav a.down').on('click',function(event) {
			event.preventDefault();
			whereTo = "down";
			pSlide();
		});
		$('#porfolioDetailHead #nav a.back').on('click',function(event) {
			event.preventDefault();
			hidePorfolioDetailsArea();
		});
		$('#porfolioDetailContainer .backtotop').on('click',function(event) {
			event.preventDefault();
			hidePorfolioDetailsArea();
		});
		
		
		$('.portfolioContent a').click(function(e) {		
			e.preventDefault();			
			showPorfolioDetailsArea();
			
			theId = this.id;
			var filter = $.deparam.fragment().filter;
						
			if ( filter === undefined ) {
				location.hash = '#filter=*';	
				return false;			
			}		
			
			$p_carousel.find('li').each(function() {
				$(this).removeClass('visible')//.children().css('opacity', '0.2');
			});				
						
			selectFirstOne();			
			checkVerticalNav();
			theId = "";		
			
			$.scrollTo( '#porfolioDetailHead', 800, {axis:'y', offset:{top:-20}} );		
			
		});
		
		
		$('#carouselContainer .nextRollOver').css('opacity', 0).mouseenter(function() {
			whereTo = "right";
			leftOrRight = true;
			pSlide();		
		}).mouseleave(function() {
			leftOrRight = false;
		});
		
		$('#carouselContainer .prevRollOver').css('opacity', 0).mouseenter(function() {
			whereTo = "left";
			leftOrRight = true;
			pSlide();	
		}).mouseleave(function() {
			leftOrRight = false;
		});
		
		
		/**IPAD**/
		if (!itsaIpad) {
			$('#carouselContainer').css({
				width: 915,
				overflow:'hidden'
			});
			
			$('#carouselContainer').touchwipe({
				preventDefaultEvents: true,
				wipeLeft: function() { $('#porfolioDetailHead #nav .next').trigger('click') },
				wipeRight: function() { $('#porfolioDetailHead #nav .prev').trigger('click') },
				wipeUp: function() { $('#porfolioDetailHead #nav .up').trigger('click') },
				wipeDown: function() { $('#porfolioDetailHead #nav .down').trigger('click') }
			});
		}
		
		
		
		
		
            /*
            $(window).bind('scrollstop', function(e){
				
                if( $('#porfolioDetailContainer').css('display') == 'block') {
					var pageTop = $("html").scrollTop();
					var contentTop = $('#porfolioDetailHead').offset().top;
					if(pageTop < contentTop) {
						$.scrollTo( '#porfolioDetailHead', 800, {axis:'y', offset:{top:-20}});
					}
				}
				
            });*/
		
		
		
		
		/* add the bg color for the list  */
		
		$('.jobContent ul li').each(function() {
			$(this).addClass('list-'+ $(this).index() );
		});
		
	}
	
	
	
	
	
	
	
	
	
	if ( bodyClass === 'portfolioDetails' || bodyClass === 'portfolioDetails nl' ) {
		
		$('.jobContent ul li').each(function() {
			$(this).addClass('list-'+ $(this).index() );
		});
		
		//PORTFOLIO items
		$('.relatedArticles a').hover(function() {
			$('h2',this).animate({
				top: -50
			},400, "easeInSine");
		}, function() {
			$('h2',this).animate({
				top: 0
			},400, "easeOutSine");
		});
		
		
		// grany anim		
		$("#grany-anim > img:eq(0)").css({"z-index": 2, top:303});
		$("#grany-anim > img:eq(1)").css({"z-index": 1, top:330, left:62});
		
		$("#grany-anim > img:eq(1)").delay(800).animate({
				top: 110
			},1000, "easeOutBack");
		
		
		// evian anim
		for(i=0; i<42; i++) {
			$("#evian-anim").prepend("<div class='evian-square'></div>");
		}
		
		$("#evian-anim").css({position:'relative', overflow:'hidden'});
		
		var row = 0;
		$("#evian-anim > div").each(function(index) {
			if(index%14 == 0 && index != 0)
				row += 1;
			$(this).css({left:41*(index-14*row)-16, top:56*row+88});
			$(this).delay(500 + index*50).fadeTo('slow', 0);
		});
		
		
		// noukies anim
		$("#noukies-anim").show();
		$("#noukies-anim #slides").slides({
			play: 4000,
			pagination: true,
			generatePagination: true,
			slideSpeed: 1500,
			pause: 2500,
			slideEasing: "easeInOutExpo",
			animationComplete: function(current){
				backgroundSwitch();
				}
			});
			
		function backgroundSwitch() {
		   
			slideId = $('#noukies-anim #slides .slide:reallyvisible').attr('id');
			$('#backgrounds img').fadeOut('slow', function(){ $(this).hide().removeClass('visible'); });
			$('#backgrounds .'+slideId).fadeIn('slow', function(){ $(this).show().addClass('visible'); });
			
		} 
		
		function histoireSlide(direction) {
			
			
			
			if ((direction === false && $('#slide').scrollLeft() <= 0) || (direction === true && $('#slide').scrollLeft() >= 3110 ))
				return;
									
			$('#slide').animate(
				{
					scrollLeft: $('#slide').scrollLeft() + ((direction) ? 1 : -1) * 20
				},
				{'duration':'fast',
				'easing':'linear',
				'complete':function() {
					histoireSlide(direction);
				}}
			);
		}
		
		
		$("#tl-hover-left").hover(
			function(e) {
				histoireSlide(false);
			},
			function(e) {
				$('#slide').stop();
			}
		);
		
		
		$("#tl-hover-right").hover(
			function(e) {
				histoireSlide(true);
			},
			function(e) {
				$('#slide').stop();
			}
		);
		
		// mercedes
		function slideSwitch() {		
			var $active = $('.mercedesSlideBanner img.active');
			if ( $active.length == 0 ) $active = $('.mercedesSlideBanner img:last');
			var $next =  $active.next().length ? $active.next() : $('.mercedesSlideBanner img:first');
			$active.addClass('last-active');
			$next.css('opacity','0').addClass('active').animate({
				opacity: 1
			}, 1000, function() {
				$active.removeClass('active last-active');
			});
		}
		slideSwitch();
		setInterval(function() {
			slideSwitch();
		}, 5000);
		
		
		//dhl
				
		$('#dhl-confettis').css({'z-index':1, left:'41px', top:'45px', opacity:0});
		
		$('#dhl-sam-sdw').css({'z-index':2, left:'-220px', top:'94px'});
		$('#dhl-sam').css({'z-index':7, left:'-300px', top:'99px'});
		
		$('#dhl-carte1').css({'z-index':6, left:'100px', top:'130px', opacity:0});
		$('#dhl-carte2').css({'z-index':5, left:'100px', top:'130px', opacity:0});
		$('#dhl-carte3').css({'z-index':4, left:'100px', top:'130px', opacity:0});
		$('#dhl-carte4').css({'z-index':3, left:'100px', top:'130px', opacity:0});
		
		
		
		$('#dhl-sam-sdw').delay(500).animate({left: '60px'}, 1000, 'easeOutExpo');
		$('#dhl-sam').delay(500).animate({left: '55px'}, 1000, 'easeOutExpo', function() {
			$('#dhl-carte1').delay(300).animate({left:'167px', top:'159px', opacity:1}, 700, 'easeOutBack', function() {$('#dhl-confettis').animate({opacity:1}, 2000);});
			$('#dhl-carte2').delay(200).animate({left:'159px', top:'103px', opacity:1}, 700, 'easeOutBack');
			$('#dhl-carte3').delay(100).animate({left:'149px', top:'54px', opacity:1}, 700, 'easeOutBack');
			$('#dhl-carte4').animate({left:'72px', top:'51px', opacity:1}, 700, 'easeOutBack');
		  });
		  
		  
		  
		 // bloom
		$('#bloom-anim img').css({display:'inline', opacity:0});
		$('#bloom-anim img:last').css({width:10, height:10, left:211, top:157}).delay(500).animate({opacity:1, width:432, height:324, left:0, top:0}, 1000, 'easeInOutExpo', function() {
			$('#bloom-content img').each(function(index) {
				$(this).css({'z-index':index+1}).delay(index*50).animate({opacity:1});
			})			
		});
		 
		 
		 
		 // rainbow		 
		$("#rainbow-anim").append("<div class='rainbow-section1'></div>");
		$("#rainbow-anim").append("<div class='rainbow-section1'></div>");
		$("#rainbow-anim").append("<div class='rainbow-section2'></div>");
		$("#rainbow-anim").append("<div class='rainbow-section3'></div>");
		$("#rainbow-anim").append("<div class='rainbow-section4'></div>");
		$("#rainbow-anim").append("<div class='rainbow-section4'></div>");
		$("#rainbow-anim").append("<div class='rainbow-section3'></div>");
		
		 
		 $("#rainbow-anim > div").each(function(index) {
		 	 $(this).delay(500 + index*100).fadeTo('slow', 0);
		 });
		 
		 
		 // dyod
		 function dyodSlideSwitch() {		
			var $active = $('.dyodSlideBanner img.active');
			if ( $active.length == 0 ) $active = $('.dyodSlideBanner img:last');
			var $next =  $active.next().length ? $active.next() : $('.dyodSlideBanner img:first');
			$active.addClass('last-active');
			$next.css('opacity','0').addClass('active').animate({
				opacity: 1
			}, 1000, function() {
				$active.removeClass('active last-active');
			});
		}
		dyodSlideSwitch();
		setInterval(function() {
			dyodSlideSwitch();
		}, 5000);
		
		
		// peterman		
		$("#peterman-anim #slides").slides({
			play: 5000,
			slideSpeed: 1500,
			pause: 2000,
			slideEasing: "easeInOutExpo"
			});
		
		
	}	
	
	
	
	
	
	if ( bodyClass === 'testimonies' || bodyClass === 'testimonies nl' ) {
		//buzz
		function buzz() {
			$('.buzz-left-light, .buzz-right-light').animate({
				opacity: 'toggle'
			},300);
		}
		
		var t = setInterval(function() {
			buzz();
		}, 500);
	}
	
	if ( bodyClass === 'clients' || bodyClass === 'clients nl' ) {
		var clientsList = $('.clientsList li');
		
		clientsList.children('img')
			.css({ opacity: 0.30 })
			.hover(function() {
				$(this).animate({ opacity: 1 });
			}, function() {
				$(this).animate({ opacity: 0.30 });
			});
	}
	
	if ( bodyClass === 'contact' || bodyClass === 'contact nl' ) {

		var fburl = "http://graph.facebook.com/mostwanted.digital";
		$.get(fburl, function(data){
			$('#facebookCount').text(data.likes);
		},'jsonp');

	}
	
}); //end doc ready




